﻿using Microsoft.AspNetCore.Mvc;
using project_connection_SqlServer.Models;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace project_connection_SqlServer.Controllers
{
    public class HomeController : Controller
    {
        private readonly CenimaDBContext _context;

        public HomeController(CenimaDBContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {

            var genres = _context.Genres.ToList();
            var movies = new List<Movie>();
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "SELECT m.MovieID, m.Title, m.Year, CAST(m.Image AS nvarchar(100)) AS Image, "
                    + " CAST(m.Description AS nvarchar(100)) AS Description,CAST(g.Description AS nvarchar(100)) AS GenresName, g.GenreID, "
                    + " ISNULL(AVG(r.NumericRating), 0) as mark FROM Movies m "
                    + " LEFT JOIN Rates r ON r.MovieID = m.MovieID "
                    + " INNER JOIN Genres g ON g.GenreID = m.GenreID "
                    + " GROUP BY m.MovieID, m.Title, m.Year, CAST(m.Image AS nvarchar(100)), "
                    + " CAST(m.Description AS nvarchar(100)), CAST(g.Description AS nvarchar(100)), g.GenreID ";
               
                command.CommandType = CommandType.Text;

                _context.Database.OpenConnection();
                using (var result = command.ExecuteReader())
                {
                    while (result.Read())
                    {

                        movies.Add(new Movie
                        {
                            MovieId = result.GetInt32("MovieId"),
                            Title = result.GetString("Title"),
                            Year = result.GetInt32("Year"),
                            Image = result.GetString("Image"),
                            Description = result.GetString("Description"),
                            Genre = new Genre
                            {
                                GenreId = result.GetInt32("GenreID"),
                                Description = result.GetString("GenresName")
                            },
                            Rates = new List<Rate>() { new Rate { NumericRating = result.GetDouble("mark") } }
                        });
                    }
                }
            }
            ViewData["genres"] = genres;
            ViewData["movies"] = movies;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}