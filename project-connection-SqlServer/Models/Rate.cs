﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace project_connection_SqlServer.Models
{
    public partial class Rate
    {
        [Key]
        [Column("MovieID")]
        public int MovieId { get; set; }
        [Key]
        [Column("PersonID")]
        public int PersonId { get; set; }
        [Column(TypeName = "ntext")]
        public string? Comment { get; set; }
        public double? NumericRating { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Time { get; set; }

        [ForeignKey("MovieId")]
        [InverseProperty("Rates")]
        public virtual Movie Movie { get; set; } = null!;
        [ForeignKey("PersonId")]
        [InverseProperty("Rates")]
        public virtual Person Person { get; set; } = null!;
    }
}
