﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace project_connection_SqlServer.Models
{
    public partial class Person
    {
        public Person()
        {
            Rates = new HashSet<Rate>();
        }

        [Key]
        [Column("PersonID")]
        public int PersonId { get; set; }
        [StringLength(200)]
        public string? Fullname { get; set; }
        [StringLength(10)]
        public string? Gender { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Email { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? Password { get; set; }
        public int? Type { get; set; }
        public bool? IsActive { get; set; }

        [InverseProperty("Person")]
        public virtual ICollection<Rate> Rates { get; set; }
    }
}
